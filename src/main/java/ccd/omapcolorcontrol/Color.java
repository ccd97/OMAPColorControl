/*
 * Copyright (C) 2016 Cyprien D'cunha
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package ccd.omapcolorcontrol;

public class Color {

    private int red;
    private int green;
    private int blue;

    public Color(){
        this.setRed(Data.maxValue);
        this.setGreen(Data.maxValue);
        this.setBlue(Data.maxValue);
    }

    public void setColors(int[] c){
        this.setRed(c[0]);
        this.setGreen(c[1]);
        this.setBlue(c[2]);
    }

    public void setRed(int r){
        this.red = r;
    }

    public void setGreen(int g){
        this.green = g;
    }

    public void setBlue(int b){
        this.blue = b;
    }

    public int[] getColors(){
        return new int[]{this.getRed(), this.getGreen(), this.getBlue()};
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public boolean isBlack() {
        return red == Data.minValue && green == Data.minValue && blue == Data.minValue;
    }
}
