/*
 * Copyright (C) 2016 Cyprien D'cunha
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package ccd.omapcolorcontrol;

import com.stericson.RootShell.exceptions.RootDeniedException;
import com.stericson.RootShell.execution.Command;
import com.stericson.RootTools.RootTools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeoutException;

class SysFS {

    static public Color readColor(){
        Color color = new Color();
        color.setColors(getColorsFromString(read(32,Data.COLOR_FILE)));
        return color;
    }

    static public boolean readCtrl(){
        return read(1,Data.CTRL_FILE).contains("1");
    }

    private static String read(int sizeOfBuffer, String file){
        String output = null;
        try {
            Process process = Runtime.getRuntime().exec(getReadCommand(file));
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));

            int read;
            char[] buffer = new char[sizeOfBuffer];
            StringBuilder sb = new StringBuilder();
            while ((read = br.read(buffer)) > 0) {
                sb.append(buffer, 0, read);
            }
            br.close();
            process.waitFor();
            output = sb.toString();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return output;
    }

    static public void write(Color color) {
        try {
            Command command = new Command(0,getCommand(color));
            RootTools.getShell(true).add(command);
        } catch (IOException | TimeoutException | RootDeniedException e) {
            e.printStackTrace();
        }
    }

    static public void write(boolean enable){
        try {
            Command command = new Command(0,getCommand(enable));
            RootTools.getShell(true).add(command);
        } catch (IOException | TimeoutException | RootDeniedException e) {
            e.printStackTrace();
        }
    }

    private static String getReadCommand(String file){
        return "cat "+file;
    }

    private static String getCommand(Color color){
        int[] c = color.getColors();
        return "echo " + getColorString(c) + " > " + Data.COLOR_FILE;
    }

    private static String getCommand(boolean enable){
        if(enable) return "echo 1 > " + Data.CTRL_FILE;
        else return "echo 0 > " + Data.CTRL_FILE;
    }

    private static String getColorString(int[] c){
        return c[0]+" 0 0 0 "+c[1]+" 0 0 0 "+c[2];
    }

    private static int[] getColorsFromString(String s){
        int[] c = new int[3];
        String[] sString = s.split("\\s+");
        c[0] = Integer.parseInt(sString[0]);
        c[1] = Integer.parseInt(sString[4]);
        c[2] = Integer.parseInt(sString[8]);
        return c;
    }
}
