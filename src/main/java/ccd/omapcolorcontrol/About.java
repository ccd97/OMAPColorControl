/*
 * Copyright (C) 2016 Cyprien D'cunha
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package ccd.omapcolorcontrol;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frame = new FrameLayout(this);
        frame.setId(View.generateViewId());
        setContentView(frame, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        getFragmentManager()
                .beginTransaction()
                .add(frame.getId(), new preferenceFragment())
                .commit();

    }

    public static class preferenceFragment extends PreferenceFragment
            implements Preference.OnPreferenceClickListener {

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.activity_about);

            for(int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++) {
                PreferenceCategory cat = (PreferenceCategory)
                        getPreferenceScreen().getPreference(i);
                for(int j = 0; j < cat.getPreferenceCount(); j++){
                    Preference pref = cat.getPreference(j);
                    pref.setOnPreferenceClickListener(this);
                }
            }
        }

        @Override
        public boolean onPreferenceClick(Preference preference) {
            String url;
            switch (preference.getKey()){
                case "author":
                    url = "http://forum.xda-developers.com/member.php?u=5767426";
                    break;
                case "sc":
                    url = "https://github.com/CCD-1997/OMAPColorControl";
                    break;
                case "licence":
                    url = "http://www.apache.org/licenses/LICENSE-2.0";
                    break;
                case "rt":
                    url = "https://github.com/Stericson/RootTools/releases";
                    break;
                case "icon":
                    url = "https://www.iconfinder.com/tahsintahil";
                    break;
                default:
                    return false;
            }
            try{
                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            } catch (ActivityNotFoundException e){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Error").setMessage("No Internet Browser Installed")
                        .setNegativeButton("OK",null).create();
                builder.show();
            }
            return true;
        }
    }
}
