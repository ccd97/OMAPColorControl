/*
 * Copyright (C) 2016 Cyprien D'cunha
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package ccd.omapcolorcontrol;

public class Data {

    public static final int minValue = 0;
    public static final int maxValue = 255;
    public static final String COLOR_FILE = "/sys/devices/platform/omapdss/manager0/cpr_coef";
    public static final String CTRL_FILE = "/sys/devices/platform/omapdss/manager0/cpr_enable";

    public class SPKeys {
        public static final String RED_SP = "redSP";
        public static final String GREEN_SP = "greenSP";
        public static final String BLUE_SP = "blueSP";
        public static final String ENABLED_SP = "isEnabledSP";
    }

}
