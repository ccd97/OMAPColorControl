/*
 * Copyright (C) 2016 Cyprien D'cunha
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package ccd.omapcolorcontrol;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.stericson.RootTools.RootTools;


public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(!RootTools.isAccessGiven()) return;
        Color color = new Color();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        color.setRed(sp.getInt(Data.SPKeys.RED_SP,Data.minValue));
        color.setGreen(sp.getInt(Data.SPKeys.GREEN_SP,Data.minValue));
        color.setBlue(sp.getInt(Data.SPKeys.BLUE_SP,Data.minValue));

        if(!color.isBlack()) SysFS.write(color);
        else SysFS.write(new Color());

        SysFS.write(sp.getBoolean(Data.SPKeys.ENABLED_SP,true));

    }
}
