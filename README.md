# OMAPColorControl

### About

A Simple Android App to Configure Color-Phase-Rotation (CPR) on OMAP4 devices with ROMs (Eg. Slim, Omni, AOSP, etc.) not having proper interface to configure it

### Licence

This app is Licensed under the Apache License, Version 2.0 (the "License")
